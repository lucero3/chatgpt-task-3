package com.example.project.chatGPTtask3;

import com.example.project.chatGPTtask3.models.Product;
import com.example.project.chatGPTtask3.models.ShoppingCartItem;
import com.example.project.chatGPTtask3.models.User;
import com.example.project.chatGPTtask3.repositories.ShoppingCartRepository;
import com.example.project.chatGPTtask3.services.ShoppingCartService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ShoppingCartServiceTest {

    @Mock
    private ShoppingCartRepository shoppingCartItemRepository;

    @InjectMocks
    private ShoppingCartService shoppingCartService;

    @Test
    public void testAddToCart() {
        // Given
        User user = new User();
        Product product = new Product();
        int quantity = 2;
        ShoppingCartItem item = new ShoppingCartItem();
        item.setUser(user);
        item.setProduct(product);
        item.setQuantity(quantity);

        // Mock the behavior of shoppingCartItemRepository.save()
        when(shoppingCartItemRepository.save(item)).thenReturn(item);

        // When
        shoppingCartService.addToCart(user, product, quantity);

        // Then
        verify(shoppingCartItemRepository).save(item);
    }

    @Test
    public void testUpdateCart() {
        // Given
        Long itemId = 1L;
        int quantity = 5;
        ShoppingCartItem item = new ShoppingCartItem();
        item.setId(itemId);
        item.setQuantity(quantity);

        // Mock the behavior of shoppingCartItemRepository.findById()
        when(shoppingCartItemRepository.findById(itemId)).thenReturn(Optional.of(item));

        // When
        shoppingCartService.updateCart(itemId, quantity);

        // Then
        verify(shoppingCartItemRepository).findById(itemId);
        verify(shoppingCartItemRepository).save(item);
    }

    @Test
    public void testRemoveFromCart() {
        // Given
        Long itemId = 1L;

        // When
        shoppingCartService.removeFromCart(itemId);

        // Then
        verify(shoppingCartItemRepository).deleteById(itemId);
    }

    @Test
    public void testGetCartItems() {
        // Given
        User user = new User();
        ShoppingCartItem item1 = new ShoppingCartItem();
        ShoppingCartItem item2 = new ShoppingCartItem();
        List<ShoppingCartItem> items = Arrays.asList(item1, item2);

        // Mock the behavior of shoppingCartItemRepository.findByUser()
        when(shoppingCartItemRepository.findByUser(user)).thenReturn(items);

        // When
        List<ShoppingCartItem> result = shoppingCartService.getCartItems(user);

        // Then
        assertEquals(2, result.size());
        assertEquals(items, result);
    }
}
