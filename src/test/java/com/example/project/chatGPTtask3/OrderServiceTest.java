package com.example.project.chatGPTtask3;

import com.example.project.chatGPTtask3.models.Order;
import com.example.project.chatGPTtask3.repositories.OrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
public class OrderServiceTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testGetAllOrders() {
        // Given
        Order order1 = new Order();
        Order order2 = new Order();
        orderRepository.save(order1);
        orderRepository.save(order2);

        // When
        List<Order> orderList = orderRepository.findAll();

        // Then
        assertEquals(2, orderList.size());
        assertTrue(orderList.contains(order1));
        assertTrue(orderList.contains(order2));
    }

    @Test
    public void testCreateOrder() {
        // Given
        Order order = new Order();

        // When
        Order savedOrder = orderRepository.save(order);

        // Then
        assertNotNull(savedOrder.getId());
    }

    @Test
    public void testUpdateOrder() {
        // Given
        Order order = new Order();
        orderRepository.save(order);

        // When
        order.setStatus("Shipped");
        Order updatedOrder = orderRepository.save(order);

        // Then
        assertEquals("Shipped", updatedOrder.getStatus());
    }

    @Test
    public void testDeleteOrder() {
        // Given
        Order order = new Order();
        orderRepository.save(order);

        // When
        orderRepository.delete(order);

        // Then
        assertFalse(orderRepository.existsById(order.getId()));
    }



}
