package com.example.project.chatGPTtask3;

import com.example.project.chatGPTtask3.models.User;
import com.example.project.chatGPTtask3.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testGetAllUsers() {
        // Given
        User user1 = new User("John", "Doe", "john@example.com");
        User user2 = new User("Alice", "Smith", "alice@example.com");

        userRepository.save(user1);
        userRepository.save(user2);

        // When
        List<User> userList = userRepository.findAll();

        // Then
        assertEquals(2, userList.size());
        assertTrue(userList.stream().anyMatch(u -> u.getFirstname().equals("John")));
        assertTrue(userList.stream().anyMatch(u -> u.getFirstname().equals("Alice")));
    }

    @Test
    public void testSaveUser() {
        // Given
        User user = new User("John", "Doe", "john@example.com");

        // When
        User savedUser = userRepository.save(user);

        // Then
        assertNotNull(savedUser.getId());
        assertEquals("John", savedUser.getFirstname());
    }

    @Test
    public void testUpdateUser() {
        // Given
        User user = new User("John", "Doe", "john@example.com");
        userRepository.save(user);

        // When
        user.setLastname("Smith");
        User updatedUser = userRepository.save(user);

        // Then
        assertEquals("Smith", updatedUser.getLastname());
    }

    @Test
    public void testDeleteUser() {
        // Given
        User user = new User("John", "Doe", "john@example.com");
        userRepository.save(user);

        // When
        userRepository.delete(user);

        // Then
        assertFalse(userRepository.existsById(user.getId()));
    }


}
