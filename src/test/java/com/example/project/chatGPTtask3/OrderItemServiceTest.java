package com.example.project.chatGPTtask3;

import com.example.project.chatGPTtask3.models.OrderItem;
import com.example.project.chatGPTtask3.repositories.OrderItemRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
public class OrderItemServiceTest {

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Test
    public void testGetAllOrderItems() {
        // Given
        OrderItem orderItem1 = new OrderItem();
        OrderItem orderItem2 = new OrderItem();
        orderItemRepository.save(orderItem1);
        orderItemRepository.save(orderItem2);

        // When
        List<OrderItem> orderItemList = orderItemRepository.findAll();

        // Then
        assertEquals(2, orderItemList.size());
        assertTrue(orderItemList.contains(orderItem1));
        assertTrue(orderItemList.contains(orderItem2));
    }

    @Test
    public void testCreateOrderItem() {
        // Given
        OrderItem orderItem = new OrderItem();

        // When
        OrderItem savedOrderItem = orderItemRepository.save(orderItem);

        // Then
        assertNotNull(savedOrderItem.getId());
    }

    @Test
    public void testUpdateOrderItem() {
        // Given
        OrderItem orderItem = new OrderItem();
        orderItemRepository.save(orderItem);

        // When
        orderItem.setQuantity(5);
        OrderItem updatedOrderItem = orderItemRepository.save(orderItem);

        // Then
        assertEquals(5, updatedOrderItem.getQuantity());
    }

    @Test
    public void testDeleteOrderItem() {
        // Given
        OrderItem orderItem = new OrderItem();
        orderItemRepository.save(orderItem);

        // When
        orderItemRepository.delete(orderItem);

        // Then
        assertFalse(orderItemRepository.existsById(orderItem.getId()));
    }

}
