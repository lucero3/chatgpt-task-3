package com.example.project.chatGPTtask3;

import com.example.project.chatGPTtask3.models.Product;
import com.example.project.chatGPTtask3.repositories.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testSaveProduct() {
        // Given
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("This is a test product");
        product.setPrice(BigDecimal.valueOf(19.99));
        product.setStockQuantity(100);

        // When
        Product savedProduct = productRepository.save(product);

        // Then
        assertEquals("Test Product", savedProduct.getName());
    }

    @Test
    public void testGetAllProducts() {
        // Given
        Product product1 = new Product();
        product1.setName("Product 1");
        product1.setDescription("Description for Product 1");
        product1.setPrice(BigDecimal.valueOf(10.99));
        product1.setStockQuantity(50);

        Product product2 = new Product();
        product2.setName("Product 2");
        product2.setDescription("Description for Product 2");
        product2.setPrice(BigDecimal.valueOf(15.99));
        product2.setStockQuantity(30);

        productRepository.save(product1);
        productRepository.save(product2);

        // When
        List<Product> productList = productRepository.findAll();

        // Then
        assertTrue(productList.stream().anyMatch(p -> p.getName().equals("Product 1")));
        assertTrue(productList.stream().anyMatch(p -> p.getName().equals("Product 2")));
    }

    @Test
    public void testUpdateProduct() {
        // Given
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("This is a test product");
        product.setPrice(BigDecimal.valueOf(19.99));
        product.setStockQuantity(100);
        Product savedProduct = productRepository.save(product);

        // When
        savedProduct.setPrice(BigDecimal.valueOf(29.99));
        Product updatedProduct = productRepository.save(savedProduct);

        // Then
        assertNotNull(updatedProduct);
        assertEquals(BigDecimal.valueOf(29.99), updatedProduct.getPrice());
    }

    @Test
    public void testDeleteProduct() {
        // Given
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("This is a test product");
        product.setPrice(BigDecimal.valueOf(19.99));
        product.setStockQuantity(100);
        Product savedProduct = productRepository.save(product);

        // When
        productRepository.delete(savedProduct);

        // Then
        assertFalse(productRepository.existsById(savedProduct.getId()));
    }


}
