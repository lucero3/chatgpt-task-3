package com.example.project.chatGPTtask3.controllers;

import com.example.project.chatGPTtask3.models.Product;
import com.example.project.chatGPTtask3.models.ShoppingCartItem;
import com.example.project.chatGPTtask3.models.User;
import com.example.project.chatGPTtask3.services.ProductService;
import com.example.project.chatGPTtask3.services.ShoppingCartService;
import com.example.project.chatGPTtask3.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/api/cart")
public class ShoppingCartController {

    private ShoppingCartService shoppingCartService;
    private UserService userService;
    private ProductService productService;

    @PostMapping("/add")
    public void addToCart(@RequestParam Long userId, @RequestParam Long productId, @RequestParam int quantity) {
        Optional<User> user = userService.getUserById(userId);
        Optional<Product> product = productService.getProductById(productId);
        shoppingCartService.addToCart(user.orElse(null), product.orElse(null), quantity);
    }

    @PutMapping("/update/{itemId}")
    public void updateCartItem(@PathVariable Long itemId, @RequestParam int quantity) {
        shoppingCartService.updateCart(itemId, quantity);
    }

    @DeleteMapping("/remove/{itemId}")
    public void removeCartItem(@PathVariable Long itemId) {
        shoppingCartService.removeFromCart(itemId);
    }

    @GetMapping("/{userId}")
    public List<ShoppingCartItem> getCartItems(@PathVariable Long userId) {
        Optional<User> user = userService.getUserById(userId);
        return shoppingCartService.getCartItems(user.orElse(null));
    }

}
