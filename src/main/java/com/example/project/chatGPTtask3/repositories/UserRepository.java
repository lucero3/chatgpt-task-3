package com.example.project.chatGPTtask3.repositories;

import com.example.project.chatGPTtask3.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
