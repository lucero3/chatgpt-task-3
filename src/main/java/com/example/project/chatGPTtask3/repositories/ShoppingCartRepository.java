package com.example.project.chatGPTtask3.repositories;

import com.example.project.chatGPTtask3.models.ShoppingCartItem;
import com.example.project.chatGPTtask3.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCartItem, Long> {
    List<ShoppingCartItem> findByUser(User user);
}

