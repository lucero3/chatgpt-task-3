package com.example.project.chatGPTtask3.repositories;

import com.example.project.chatGPTtask3.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
