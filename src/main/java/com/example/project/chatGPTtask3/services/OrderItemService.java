package com.example.project.chatGPTtask3.services;

import com.example.project.chatGPTtask3.models.OrderItem;
import com.example.project.chatGPTtask3.repositories.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class OrderItemService {

    @Autowired
    private OrderItemRepository orderItemRepository;

    public List<OrderItem> getAllOrderItems() {
        return orderItemRepository.findAll();
    }

    public Optional<OrderItem> getOrderItemById(Long id) {
        return orderItemRepository.findById(id);
    }

    public OrderItem createOrderItem(OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }

    public OrderItem updateOrderItem(Long id, OrderItem updatedOrderItem) {
        Optional<OrderItem> existingOrderItem = orderItemRepository.findById(id);
        if (existingOrderItem.isPresent()) {
            updatedOrderItem.setId(id);
            return orderItemRepository.save(updatedOrderItem);
        } else {
            throw new RuntimeException("Order item not found with id: " + id);
        }
    }

    public void deleteOrderItem(Long id) {
        orderItemRepository.deleteById(id);
    }
}
