package com.example.project.chatGPTtask3.services;

import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.param.ChargeCreateParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {

    @Value("${stripe.currency}")
    private String currency;

    public void processPayment(String token, double amount) throws StripeException {
        ChargeCreateParams params = ChargeCreateParams.builder()
                .setAmount((long) (amount * 100)) // amount in cents
                .setCurrency(currency)
                .setSource(token)
                .build();

        Charge charge = Charge.create(params);
        if (charge.getStatus().equals("succeeded")) {
            // Payment successful
            System.out.println("Payment successful. Charge ID: " + charge.getId());
            // Perform further actions like updating order status, sending confirmation email, etc.
        } else {
            // Payment failed
            System.out.println("Payment failed.");
            // Log error details or notify user about payment failure
        }
    }
}
