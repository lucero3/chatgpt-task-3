package com.example.project.chatGPTtask3.services;

import com.example.project.chatGPTtask3.models.Product;
import com.example.project.chatGPTtask3.models.ShoppingCartItem;
import com.example.project.chatGPTtask3.models.User;
import com.example.project.chatGPTtask3.repositories.ShoppingCartRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    public void addToCart(User user, Product product, int quantity) {
        ShoppingCartItem item = new ShoppingCartItem();
        item.setUser(user);
        item.setProduct(product);
        item.setQuantity(quantity);
        shoppingCartRepository.save(item);
    }

    public void updateCart(Long itemId, int quantity) {
        ShoppingCartItem item = shoppingCartRepository.findById(itemId)
                .orElseThrow(() -> new RuntimeException("Shopping cart item not found with id: " + itemId));
        item.setQuantity(quantity);
        shoppingCartRepository.save(item);
    }

    public void removeFromCart(Long itemId) {
        shoppingCartRepository.deleteById(itemId);
    }

    public List<ShoppingCartItem> getCartItems(User user) {
        return shoppingCartRepository.findByUser(user);
    }
}

